<?php

/**
 * This is project's console commands configuration for Robo task runner.
 *
 * @see http://robo.li/
 */

use Robo\Tasks;

/**
 * Robo file.
 */
class RoboFile extends Tasks {

  /**
   * Run tests.
   */
  public function test(array $args) {
    return $this->taskPHPUnit()
      ->args($args)
      ->run();
  }

  /**
   * Static code analysis.
   */
  public function phpstan(array $args) {
    $cwd = dirname(__FILE__);
    return $this->taskExec("./vendor/bin/phpstan analyze src")
      ->args($args)
      ->run();
  }

  /**
   * Check coding standards.
   */
  public function phpcs() {
    $cwd = dirname(__FILE__);
    return $this->taskExec("./vendor/bin/phpcs --runtime-set installed_paths {$cwd}/vendor/drupal/coder/coder_sniffer --standard=Drupal --extensions=php,module,inc,install,profile,theme --ignore=.git,vendor,assets {$cwd}")
      ->run();
  }

  /**
   * Fix coding standards.
   */
  public function phpcbf() {
    $cwd = dirname(__FILE__);
    return $this->taskExec("./vendor/bin/phpcbf --runtime-set installed_paths {$cwd}/vendor/drupal/coder/coder_sniffer --standard=Drupal --extensions=php,module,inc,install,profile,theme --ignore=.git,vendor,assets {$cwd}")
      ->run();
  }

}
