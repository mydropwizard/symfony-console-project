Symfony Console Template
========================

This is a project template for creating Symfony Console apps.

Usage
-----

```
composer create-project mydropwizard/symfony-console-project:dev-master some-dir --no-interaction
```

