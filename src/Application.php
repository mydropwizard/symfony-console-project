<?php

namespace ConsoleApp;

use myDropWizard\ConsoleUtils\Application as UpstreamApplication;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Console application class.
 */
class Application extends UpstreamApplication {

  /**
   * The application name.
   *
   * @var string
   */
  const NAME = 'ConsoleApp';

  /**
   * The application version.
   *
   * @var string
   */
  const VERSION = '0.1.0';

  /**
   * Constructs an Application.
   *
   * @param string $app_root
   *   The root of the application.
   * @param string $local_root
   *   The root of where the user is running the application.
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The container.
   */
  public function __construct($app_root, $local_root, ContainerInterface $container) {
    parent::__construct($this::NAME, $this::VERSION, $app_root, $local_root, $container);
  }

}
