<?php

namespace ConsoleApp\Exception;

/**
 * Generic exception class for ConsoleApp.
 */
class ConsoleAppException extends \RuntimeException {

}
