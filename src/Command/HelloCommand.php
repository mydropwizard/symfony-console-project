<?php

namespace ConsoleApp\Command;

use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * An example command to get you started.
 */
class HelloCommand extends Command {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a HelloCommand.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger.
   */
  public function __construct(LoggerInterface $logger) {
    parent::__construct();
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  protected function configure() {
    $this->setName("hello")
      ->setDescription("Says hello");
  }

  /**
   * {@inheritdoc}
   */
  protected function execute(InputInterface $input, OutputInterface $output) {
    $this->logger->debug('About to say hello');

    $output->writeln('Hello!');

    $this->logger->debug('I just said hello');
  }

}
