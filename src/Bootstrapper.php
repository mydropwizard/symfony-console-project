<?php

namespace ConsoleApp;

use myDropWizard\ConsoleUtils\Bootstrapper as UpstreamBootstrapper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Bootstraps the application.
 */
class Bootstrapper extends UpstreamBootstrapper {

  /**
   * {@inheritdoc}
   */
  protected function createApplication($app_root, $local_root, ContainerInterface $container) {
    return new Application($app_root, $local_root, $container);
  }

}
