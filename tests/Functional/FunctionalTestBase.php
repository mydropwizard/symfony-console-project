<?php

namespace ConsoleApp\Tests\Functional;

use Symfony\Component\Process\Process;
use myDropWizard\ConsoleUtils\TestBase\FunctionalTestBase as UpstreamFunctionalTestBase;

/**
 * Base class for functional tests.
 */
abstract class FunctionalTestBase extends UpstreamFunctionalTestBase {

  /**
   * Get process for running the app.
   *
   * @param string[] $args
   *   The arguments to run the app with.
   *
   * @return \Symfony\Component\Process\Process
   *   The process to run.
   */
  protected function createProcess(array $args) {
    $args = array_merge([dirname(dirname(__DIR__)) . '/bin/cli'], $args);
    $process = new Process($args);
    return $process;
  }

}
