<?php

namespace ConsoleApp\Tests\Functional;

/**
 * Simple smoke test to make sure the ConsoleApp runs at a rudimentary level.
 */
class SmokeFunctionalTest extends FunctionalTestBase {

  /**
   * Tests that we can run the app.
   */
  public function testSmoke() {
    $process = $this->createProcess(['hello']);
    $process->mustRun();

    $this->assertEquals("Hello!\n", $process->getOutput());
  }

}
