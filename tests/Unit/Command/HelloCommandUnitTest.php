<?php

namespace ConsoleApp\Tests\Unit\Command;

use ConsoleApp\Command\HelloCommand;
use myDropWizard\ConsoleUtils\TestBase\UnitTestBase;
use Psr\Log\LoggerInterface;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

/**
 * Unit test for the HelloCommand class.
 */
class HelloCommandUnitTest extends UnitTestBase {

  protected $logger;

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    $this->logger = $this->prophesize(LoggerInterface::class);
  }

  /**
   * Tests the HelloCommand::execute() method.
   */
  public function testExecute() {
    $command = new HelloCommand($this->logger->reveal());

    $input = $this->prophesize(InputInterface::class);
    $output = $this->prophesize(OutputInterface::class);

    $this->logger->debug('About to say hello')
      ->shouldBeCalled();
    $output->writeln('Hello!')
      ->shouldBeCalled();
    $this->logger->debug('I just said hello')
      ->shouldBeCalled();

    $this->invokeMethod($command, 'execute', [$input->reveal(), $output->reveal()]);
  }

}
